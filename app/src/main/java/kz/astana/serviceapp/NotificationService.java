package kz.astana.serviceapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.core.app.NotificationCompat;

public class NotificationService extends Service {

    private final String NOTIFICATION_CHANNEL_ID = "NOTIFICATION_CHANNEL_ID";
    private final int NOTIFY_ID = 100;
    private final int REQUEST_CODE = 1000;
    private NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Log.d("Hello", "onCreate");
        createChannel();
        Log.d("Hello", "createChannel");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("Hello", "Sleep");
                    TimeUnit.SECONDS.sleep(10);
                    Log.d("Hello", "Wake up");
                    sendNotification();
                    Log.d("Hello", "Notification sent");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Service", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("Channel for service");
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void sendNotification() {
        Intent intent = new Intent(NotificationService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                NotificationService.this,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(NotificationService.this, NOTIFICATION_CHANNEL_ID);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle("Service notification");
        builder.setContentText("It's out notification from service");
        builder.setAutoCancel(true);

        Notification notification = builder.build();
        Log.d("Hello", "Notify");
        notificationManager.notify(NOTIFY_ID, notification);
        Log.d("Hello", "Notified");
    }
}