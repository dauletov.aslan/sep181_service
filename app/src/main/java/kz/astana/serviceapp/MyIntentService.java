package kz.astana.serviceapp;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;

public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("name");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Hello", "onCreate(IntentService)");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int time = intent.getIntExtra("TIME_EXTRA", 0);
        String text = intent.getStringExtra("TEXT_EXTRA");
        Log.d("Hello", "Start: " + text);
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("Hello", "End: " + text);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(IntentService)");
    }
}
