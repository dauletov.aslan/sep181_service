package kz.astana.serviceapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "onCreate(Activity)");
        setContentView(R.layout.activity_main);

        Button start = findViewById(R.id.startButton);
        Button stop = findViewById(R.id.stopButton);
        Button sendNotification = findViewById(R.id.sendNotification);
        Button sendIntentExtra = findViewById(R.id.sendIntentExtra);

        Intent intent = new Intent(MainActivity.this, MyService.class);
        intent.putExtra("TIME_EXTRA", 5);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(intent);
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(intent);
            }
        });

        sendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(MainActivity.this, NotificationService.class));
            }
        });

        sendIntentExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MyIntentService.class);
                i.putExtra("TIME_EXTRA", 3).putExtra("TEXT_EXTRA", "Call 1");
                startService(i);
                i.putExtra("TIME_EXTRA", 1).putExtra("TEXT_EXTRA", "Call 2");
                startService(i);
                i.putExtra("TIME_EXTRA", 5).putExtra("TEXT_EXTRA", "Call 3");
                startService(i);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Hello", "onStart(Activity)");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Hello", "onPause(Activity)");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(Activity)");
    }
}