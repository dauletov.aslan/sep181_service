package kz.astana.serviceapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Hello", "onCreate(Service)");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Hello", "onStartCommand(Service)");
        int time = intent.getIntExtra("TIME_EXTRA", 0);
        doSomeTask(time);
        Log.d("Hello", "Service #" + startId);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(Service)");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Hello", "onBind(Service)");
        return null;
    }

    private void doSomeTask(int time) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= time; i++) {
                    Log.d("Hello", "Counting: " + i);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }).start();
    }
}